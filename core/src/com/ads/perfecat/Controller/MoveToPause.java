package com.ads.perfecat.Controller;

import com.ads.perfecat.PerfecatGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by PC on 06.05.2016.
 */
public class MoveToPause extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().MoveToPauseScreen();
    }
}