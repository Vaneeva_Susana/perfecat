package com.ads.perfecat.Controller;

import com.ads.perfecat.PerfecatGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by 05k1103 on 09.04.2016.
 */
public class NewDispose  extends ClickListener {
    public void clickedHochuIgrat(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().HocuIgrat();
    }
    public void clickedHochuSpat(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().HochuSpat();
    }
    public void clickedMoloko(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().Moloko();
    }
    public void clickedMyaso(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().Myaso();
    }
    public void clickedVoda(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().Voda();
    }
    public void clickedKorm(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().Korm();
    }
}
