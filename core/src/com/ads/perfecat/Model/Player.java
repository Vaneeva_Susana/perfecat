package com.ads.perfecat.Model;

import com.ads.perfecat.PerfecatGame;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Base64;
import java.util.Random;

import static com.ads.perfecat.Model.PlayerState.*;


/**
 * Created by 05k1103 on 02.04.2016.
 */
public class Player extends Actor {
    Animation walkLeft;
    Animation walkRight;
    Animation runLeft;
    Animation runRight;
    Animation playWithBall;
    Animation playWithBall2;
    Animation sit;
    Animation current;
    Animation slip;
    PlayerState state;
    public float currentTime;


    public Player(float x, float y) {
        setPosition(x, y);
        setSize(200, 200);
        sit = new Animation(0.2f,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/ActorSit.png")),
                        new TextureRegion(new Texture("android/assets/ActorSit.png")),
                        new TextureRegion(new Texture("android/assets/ActorSit.png")),
                        new TextureRegion(new Texture("android/assets/ActorSit.png"))
                }
        );
        walkLeft = new Animation(0.2f,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo1L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo2L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo1L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo2L.png"))
                }
        );
        walkRight = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo1R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo2R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo1R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationGo/ActionGo2R.png"))
                }
        );
        runLeft = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun1L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun2L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun3L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun4L.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun5L.png"))
                }
        );
        runRight = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun1R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun2R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun3R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun4R.png")),
                        new TextureRegion(new Texture("android/assets/AnimationRun/ActionRun5R.png"))
                }
        );
        playWithBall = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay1.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay2.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay1.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay2.png"))
                }

        );

        playWithBall2 = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay3.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay4.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay3.png")),
                        new TextureRegion(new Texture("android/assets/ActorPLAY/ActorPlay4.png"))
                }
        );
        slip = new Animation(0.2F,
                new TextureRegion[]{
                        new TextureRegion(new Texture("android/assets/ActorSlip.png")),
                        new TextureRegion(new Texture("android/assets/ActorSlip.png")),
                        new TextureRegion(new Texture("android/assets/ActorSlip.png")),
                        new TextureRegion(new Texture("android/assets/ActorSlip.png"))
                }
        );

        setState(SIT);
    }

    public void act(float delta) {
        currentTime += delta;
        currentTime %= current.getAnimationDuration();
        switch (state) {
            case SIT:
                break;
            case WALKLEFT:
                moveBy(20*delta, 0);
                if (getX() + getWidth() > Gdx.graphics.getWidth())
                    setX(Gdx.graphics.getWidth() - getWidth());
                break;
            case WALKRIGHT:
                moveBy(-20*delta, 0);
                if (getX() < 0)
                    setX(0);
                break;
            case RUNLEFT:
                moveBy(50*delta, 0);
                if (getX() + getWidth() > Gdx.graphics.getWidth())
                    setX(Gdx.graphics.getWidth() - getWidth());
                break;
            case RUNRIGHT:
                moveBy(-50*delta, 0);
                if (getX() < 0)
                    setX(0);
                break;
            case PLAY:
                break;
            case SLIP:
                break;
            case PLAY2:
                break;
        }
    }


    public void setState(PlayerState state) {
        this.state = state;
        switch (state) {
            case SIT:
                current = sit;
                break;
            case WALKLEFT:
                current = walkLeft;
                break;
            case WALKRIGHT:
                current = walkRight;
                break;
            case RUNLEFT:
                current = runLeft;
                break;
            case RUNRIGHT:
                current = runRight;
                break;
            case PLAY:
                current = playWithBall;
                break;
            case SLIP:
                current = slip;
                break;
            case PLAY2:
                current = playWithBall2;
                break;
        }
        currentTime = 0;
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(current.getKeyFrame(currentTime), getX(), getY());
    }
}

