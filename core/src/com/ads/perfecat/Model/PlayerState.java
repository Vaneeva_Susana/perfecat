package com.ads.perfecat.Model;

/**
 * Created by 05k1103 on 30.04.2016.
 */
public enum PlayerState {
    RUNLEFT,
    RUNRIGHT,
    WALKLEFT,
    WALKRIGHT,
    PLAY,
    PLAY2,
    SIT,
    SLIP
}
