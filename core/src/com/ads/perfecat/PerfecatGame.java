package com.ads.perfecat;

import com.ads.perfecat.Screens.GameOverScreen;
import com.ads.perfecat.Screens.GameScreen;
import com.ads.perfecat.Screens.MainMenuScreen;
import com.ads.perfecat.Screens.PauseScreen;
import com.ads.perfecat.Screens.SettingsScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by PC on 20.02.2016.
 */
public class PerfecatGame extends Game  {

        public static final float WORLD_WIDTH = 1920f; //ширина мира
        public static final float WORLD_HEIGHT = 1040f; //высота мира

    private static PerfecatGame ourInstance = new PerfecatGame();
    private MainMenuScreen screen;

    public static PerfecatGame getInstance() {
        return ourInstance;
    }
    private PerfecatGame() {    }

    private float ppuX; //коэффициент подобия по иксу
    private float ppuY; //коэффициент подобия по игреку

    private SpriteBatch batch;
    private ShapeRenderer shape;
    private BitmapFont font;
    private SettingsScreen settingsScreen;
    private GameScreen gameScreen;
    private MainMenuScreen mainMenuScreen;
    private GameOverScreen gameOverScreen;
    private PauseScreen pauseScreen;

    public ShapeRenderer getShapeRenderer() {
        return shape;
    }


    @Override
    public void create() {
        ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
        ppuY = Gdx.graphics.getHeight()/WORLD_HEIGHT;
        loadGraphics(); //загрузить графику
        batch = new SpriteBatch(); //позволяет отрисовывать текстуры
        shape = new ShapeRenderer(); //позволяет рисовать графические примитивы
        font = new BitmapFont(); //позволяет рисовать текст разными шрифтами
        mainMenuScreen = new MainMenuScreen(batch, shape, font);
        settingsScreen = new SettingsScreen(batch, shape, font);
        gameScreen = new GameScreen(batch, shape, font);
        gameOverScreen = new GameOverScreen(batch, shape, font);
        pauseScreen = new PauseScreen(batch, shape, font);
        MoveToMainMenu();
//        gameScreen.Hochu();
    }

    public void MoveToMainMenu() {
        setScreen(mainMenuScreen );
    }
    public void MoveToSettings(){
        setScreen(settingsScreen);
    }
    public void moveToGame(){ setScreen(gameScreen);}
    public void MoveToGame() {
        setScreen(gameScreen = new GameScreen(batch, shape, font));

    }
    public void MoveToGameOver() { setScreen( gameOverScreen);    }
    public void MoveToPauseScreen() { setScreen(pauseScreen);    }

    public void Voda()  {}
    public void Korm() { }
    public void Moloko() { }
    public void Myaso() { }
    public void HocuIgrat() { }
    public void HochuSpat() { }


    private void loadGraphics() {
    }

    public float getPpuX() {
        return ppuX;
    }
    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }
    public float getPpuY() {
        return ppuY;
    }
    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }


}
