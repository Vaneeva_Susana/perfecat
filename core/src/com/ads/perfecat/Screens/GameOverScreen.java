package com.ads.perfecat.Screens;

import com.ads.perfecat.Controller.MoveToGame;
import com.ads.perfecat.Controller.MoveToMainMenu;
import com.ads.perfecat.Controller.MoveToSettingsMenu;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by PC on 06.05.2016.
 */
public class GameOverScreen implements Screen {

    private ImageActor BackGround;
    private ImageActor btnStart;
    private Stage stage;


    public GameOverScreen(SpriteBatch batch, ShapeRenderer shape, BitmapFont font) {
        BackGround = new ImageActor(new Texture("android/assets/Screens/GAMEOVER.png"), 0, 0);
        btnStart = new ImageActor(new Texture("android/assets/BTN/START.png"), 800, 100);
        btnStart.addListener(new MoveToMainMenu());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);


        stage.addActor(BackGround);
        stage.addActor(btnStart);
    }

    @Override
    public void show() {Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}