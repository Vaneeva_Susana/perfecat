package com.ads.perfecat.Screens;

import com.ads.perfecat.Controller.MoveToGame;
import com.ads.perfecat.Controller.MoveToGameOver;
import com.ads.perfecat.Controller.MoveToPause;
import com.ads.perfecat.Controller.MoveToSettingsMenu;
import com.ads.perfecat.Model.Player;
import com.ads.perfecat.Model.PlayerState;
import com.ads.perfecat.Model.World;
import com.ads.perfecat.PerfecatGame;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by PC on 26.02.2016.
 */
public class GameScreen implements Screen {
    private Stage stage;
    private ImageActor pause;
    private ImageActor Korm;
    private ImageActor HochuIgrat;
    private ImageActor HochuSpat;
    private ImageActor Moloko;
    private ImageActor Myaso;
    private ImageActor Voda;
    private ImageActor backGround;
    private ImageActor Kakulya;
    private ImageActor Pochinka;
    private ImageActor WantToKorm;
    private ImageActor WantToIgrat;
    private ImageActor WantToVoda;
    private ImageActor WantToMyaso;
    private ImageActor WantToSpat;
    private ImageActor WantToMoloko;
    private ImageActor VosklZnak;
    private Player ActorSit;
    public int Ochki = 0;
    int Live = 3;
    Timer timer1 = new Timer();
    Timer timer2 = new Timer();
    Timer timer3 = new Timer();

    public float eventTime = 5.0f; //как часто будет всплывать событие
    public float pastTime = 0;


    Random rx = new Random();
    Random ry = new Random();


    public GameScreen(SpriteBatch batch, ShapeRenderer shape, BitmapFont font) {

        float cellCize = 160f;
        float tmpX = PerfecatGame.WORLD_WIDTH - cellCize;
        float tmpY = PerfecatGame.WORLD_HEIGHT / 5f;


        ActorSit = new Player(500, 60);
        backGround = new ImageActor(new Texture("android/assets/Screens/GameScreen.png"), 0, 0);
        VosklZnak = new ImageActor(new Texture("android/assets/ZNAK/VosklZnak.png"), 900,900);
        VosklZnak.setVisible(false);
        pause = new ImageActor(new Texture("android/assets/BTN/BtnPause.png"), 1500, 850);
        pause.addListener(new MoveToPause());
        pause.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                timer1.stop();
                timer2.stop();
                timer3.stop();
            }
        });
        Korm = new ImageActor(new Texture("android/assets/Hochu/Korm.png"), 0, tmpY - cellCize/2);
        Moloko = new ImageActor(new Texture("android/assets/Hochu/Moloko.png"), 0, 2 * tmpY - cellCize/2);
        Myaso = new ImageActor(new Texture("android/assets/Hochu/Myaso.png"), 0, 3 * tmpY - cellCize/2);
        Voda = new ImageActor(new Texture("android/assets/Hochu/Voda.png"), 0, 4 * tmpY - cellCize/2);
        HochuIgrat = new ImageActor(new Texture("android/assets/Hochu/HochuIgrat.png"), tmpX, 3 * tmpY - cellCize/2);
        HochuSpat = new ImageActor(new Texture("android/assets/Hochu/HochuSpat.png"), tmpX, 4 * tmpY - cellCize/2);


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(ActorSit);
        stage.addActor(pause);
        stage.addActor(VosklZnak);
        stage.addActor(Korm);
        stage.addActor(Moloko);
        stage.addActor(Myaso);
        stage.addActor(Voda);
        stage.addActor(HochuIgrat);
        stage.addActor(HochuSpat);
        /*
        OrthographicCamera uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiStage = new UserInterface(new ScreenViewport(camera), batch);
        */




    }

    /*
     public void Hochu() {
         timer = new Timer();
         timer.scheduleTask(new Timer.Task() {
             @Override
             public void run() {
                 Want();
             }
         }, 10f, 15f);
         timer.start();

     }
 */
    public void Ochki(int b){
        b = Ochki;
    }
    public void bigSwitch() {
        int n = rx.nextInt(2);
        switch (n) {
            case 0:
                Want();
                break;
            case 1:
                aLotToWant();
                break;
            case 2:
                KakulyaPochinka();
                break;
        }
    }

    public void setRandomAnimation() {
        switch (rx.nextInt(4)) {
            case 0:
                ActorSit.setState(PlayerState.RUNLEFT);
                break;
            case 1:
                ActorSit.setState(PlayerState.RUNRIGHT);
                break;
            case 2:
                ActorSit.setState(PlayerState.SIT);
                break;
            case 3:
                ActorSit.setState(PlayerState.WALKLEFT);
                break;
            case 4:
                ActorSit.setState(PlayerState.WALKRIGHT);
                break;
        }
    }

    public void Want() {
        int n = rx.nextInt(3);
        switch (n) {
            case 0:
                WantToKorm = new ImageActor(new Texture("android/assets/Want/WantToKorm.png"),150,800);
                setWantToKorm(WantToKorm);
                stage.addActor(WantToKorm);
                setRandomAnimation();
                timer1();
                Korm.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {
                        if (WantToKorm != null) {
                            WantToKorm.remove();
                          ///  timer1.stop();
                            try {
                                timer1();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer1.stop();
                        }

                    }});
                break;
            case 1:
                WantToMoloko = new ImageActor(new Texture("android/assets/Want/WantToMoloko.png"),150,800);
                setWantToMoloko(WantToMoloko);
                stage.addActor(WantToMoloko);
                setRandomAnimation();
                timer1();
                Moloko.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {
                        if (WantToMoloko != null){
                            WantToMoloko.remove();
                            ///timer1.stop();
                            try {
                                timer1();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer1.stop();
                        }
                    }
                });
                break;
            case 2:
                WantToMyaso = new ImageActor(new Texture("android/assets/Want/WantToMyaso.png"),150,800);
                stage.addActor(WantToMyaso);
                setWantToMyaso(WantToMyaso);
                setRandomAnimation();
                timer1();
                Myaso.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {
                        if (WantToMyaso != null){
                            WantToMyaso.remove();
                           /// timer1.stop();
                            try {
                                timer1();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer1.stop();
                        }
                    }
                });
                break;
            case 3:
                WantToVoda = new ImageActor(new Texture("android/assets/Want/WantToVoda.png"),150,800 /* ActorSit.getX() + 120, ActorSit.getY() +2 * ActorSit.getHeight()*/);
                stage.addActor(WantToVoda);
                setwantToVoda(WantToVoda);
                setRandomAnimation();
                timer1();
                Voda.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {

                        if (WantToVoda != null){
                            WantToVoda.remove();
                           /// timer1.stop();
                            }
                            try {
                                timer1();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer1.stop();
                    }
                });
                break;
        }


    }

    public void aLotToWant() {
        int n = rx.nextInt(2);
        switch (n) {
            case 0:
                WantToIgrat = new ImageActor(new Texture("android/assets/Want/WantToIgrat.png"),150,800);
                stage.addActor(WantToIgrat);
                setWantToIgrat(WantToIgrat);
                timer2();
                HochuIgrat.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {
                        ActorSit.setState(PlayerState.PLAY);
                        if (WantToIgrat != null){
                            WantToIgrat.remove();
                            try {
                                timer2();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer2.stop();
                        }
                    }
                });
                break;
            case 1:
                WantToSpat = new ImageActor(new Texture("android/assets/Want/WantToSpat.png"),150,800);
                stage.addActor(WantToSpat);
                setWantToSpat(WantToSpat);
                timer2();
             HochuSpat.addListener(new ClickListener(){
                public void clicked(InputEvent event, float x, float y) {
                    ActorSit.setState(PlayerState.SLIP);
                    if (WantToSpat != null){
                        WantToSpat.remove();
                        try {
                            timer2();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } timer2.stop();
                    }

                }});
            break;
            case 2:
                WantToIgrat = new ImageActor(new Texture("android/assets/Want/WantToIgrat.png"),150,800);
                stage.addActor(WantToIgrat);
                setWantToIgrat(WantToIgrat);
                timer2();
                HochuIgrat.addListener(new ClickListener(){
                    public void clicked(InputEvent event, float x, float y) {
                        ActorSit.setState(PlayerState.PLAY2);
                        if (WantToIgrat != null){
                            WantToIgrat.remove();
                            try {
                                timer2();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer2.stop();
                        }
                    }
                });
                break;
        }
    }

    public void KakulyaPochinka() {

        int n = rx.nextInt(1);
        switch (n) {
            case 0:
               // VosklZnak = new ImageActor(new Texture("android/assets/ZNAK/VosklZnak.png"), 900,900);
               // stage.addActor(VosklZnak);
                Kakulya = new ImageActor(new Texture("android/assets/ZNAK/Kakulya.png"), rx.nextInt(1823), ry.nextInt(416));
                stage.addActor(Kakulya);
                setRandomAnimation();
                timer3();

                Kakulya.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        if (Kakulya != null) {
                            Kakulya.remove();
                            ///timer3.stop();
                            try {
                                timer3();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer3.stop();
                            VosklZnak.setVisible(false);
                        }
                    }
                });
               /// VosklZnak.setVisible(false);
                break;
            case 1:
               // VosklZnak = new ImageActor(new Texture("android/assets/ZNAK/VosklZnak.png"), 900,900);
               // stage.addActor(VosklZnak);
                Pochinka = new ImageActor(new Texture("android/assets/ZNAK/Pochinka.png"), rx.nextInt(1823), ry.nextInt(940));
                stage.addActor(Pochinka);
                setRandomAnimation();
                timer3();
                Pochinka.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        if (Pochinka != null) {
                            Pochinka.remove();
                            try {
                                timer3();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } timer3.stop();
                        }
                    }
                });
               /// VosklZnak.setVisible(false);
                break;
        }


    }
    public void timer1()  {
        timer1.clear();
        timer1.scheduleTask(new Timer.Task() {
            @Override
            public void run() {

                if (getExecuteTimeMillis() > 3) {
                    Live = Live -1;}
                    else{
                    Ochki =Ochki +2;
                }
                /// для основных четырех потребностей
                /// if .... Live -1 else ochki + 2

            }
        }, 4f);
        timer1.start();
        proverka(Live);
    }

    public  void timer2() {

        timer2.clear();
        timer2.scheduleTask(new Timer.Task() {
            public void run() {
                if (getExecuteTimeMillis() > 3) {
                    Live = Live - 1;}
                    else{
                    Ochki =Ochki +1;
                }
                /// Для спать/играть
                /// if .... Live -1 else ochki + 1

            }
    }, 4f);
        timer2.start();
        proverka(Live);
    }

    public void timer3() {
        VosklZnak = new ImageActor(new Texture("android/assets/ZNAK/VosklZnak.png"), 900,900);
        stage.addActor(VosklZnak);
        VosklZnak = new ImageActor(new Texture("android/assets/ZNAK/VosklZnak.png"), 900,900);
        stage.addActor(VosklZnak);
        timer3.clear();
        timer3.scheduleTask(new Timer.Task() {
            public void run() {
                if (getExecuteTimeMillis() > 5) {
                    Live =Live -1;
                VosklZnak.remove();}
                else{
                    VosklZnak.remove();
                    Ochki = Ochki+5;
                }

                /// Для какули/поломки
                /// if .... Live -1 else ochki + 5


            }
        }, 6f);
        timer3.start();
        proverka(Live);;

    }
    public void proverka(int live){
             if (live <=0) {
                 PerfecatGame.getInstance().MoveToGameOver();
             }

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        pastTime += delta;
        if(pastTime>eventTime){
            pastTime = 0;
            bigSwitch();
        }

        stage.act(delta);
        stage.draw();

    }

    public void setWantToMoloko(ImageActor wantToMoloko) {
        this.WantToMoloko = wantToMoloko;
    }

    public void setWantToIgrat(ImageActor wantToIgrat) {
        this.WantToIgrat = wantToIgrat;
    }

    public void setWantToMyaso(ImageActor wantToMyaso) {
        this.WantToMyaso = wantToMyaso;
    }

    public void setWantToSpat(ImageActor wantToSpat) {
        this.WantToSpat = wantToSpat;
    }

    public void setWantToKorm(ImageActor WantToKorm) {
        this.WantToKorm = WantToKorm;
    }


    public void setwantToVoda(ImageActor wantToVoda) {
        this.WantToVoda = wantToVoda;
    }
    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
