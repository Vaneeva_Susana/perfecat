package com.ads.perfecat.Screens;

import com.ads.perfecat.Controller.*;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by PC on 20.02.2016.
 */
public class MainMenuScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor settingsButton;

    public MainMenuScreen(SpriteBatch batch, ShapeRenderer shape, BitmapFont font) {
        backGround = new ImageActor(new Texture("android/assets/Screens/1plan.png"), 0, 0);
        playButton = new ImageActor(new Texture("android/assets/BTN/PLAY.png"), 525, 500);
        playButton.addListener(new MoveToGame());
        settingsButton = new ImageActor(new Texture("android/assets/BTN/OPTIONS.png"), 525, 240);
        settingsButton.addListener(new MoveToSettingsMenu());


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(playButton);
        stage.addActor(settingsButton);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
