package com.ads.perfecat.Screens;

import com.ads.perfecat.Controller.MoveToGame;
import com.ads.perfecat.Controller.MoveToMainMenu;
import com.ads.perfecat.Controller.MoveToSettingsMenu;
import com.ads.perfecat.PerfecatGame;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by PC on 06.05.2016.
 */
public class PauseScreen implements Screen {

    private ImageActor BackGround;
    private ImageActor btnBack;
    private ImageActor btnReplay;
    private ImageActor btnOptions;
    private Stage stage;

    public PauseScreen (SpriteBatch batch, ShapeRenderer shape, BitmapFont font){
        BackGround = new ImageActor(new Texture("android/assets/Screens/PAUSE.png"), 0, 0);
        btnBack = new ImageActor(new Texture("android/assets/BTN/BACK2.png"), 250, 250);
        btnBack.addListener(new ClickListener() {
                                public void clicked(InputEvent event, float x, float y) {
                                    PerfecatGame.getInstance().moveToGame();

                                }
                            });

        btnReplay = new ImageActor(new Texture("android/assets/BTN/AGAIN.png"), 800, 250);
        btnReplay.addListener(new MoveToMainMenu());
        btnOptions = new ImageActor(new Texture("android/assets/BTN/ForOPTIOS.png"), 1350, 250);
        btnOptions.addListener(new MoveToSettingsMenu());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(BackGround);
        stage.addActor(btnBack);
        stage.addActor(btnReplay);
        stage.addActor(btnOptions);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

            stage.dispose();
    }
}
