package com.ads.perfecat.Screens;

import com.ads.perfecat.Controller.*;
import com.ads.perfecat.PerfecatGame;
import com.ads.perfecat.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;
/**
 * Created by PC on 26.02.2016.
 */

public class SettingsScreen implements Screen {
    private ImageActor musikOn;
    private ImageActor musikOff;
    private ImageActor CatButton;
    private ImageActor backButton;
    private Stage stage;
    private ImageActor backGround;

    public SettingsScreen(SpriteBatch batch, ShapeRenderer shape, BitmapFont font) {
        backGround = new ImageActor(new Texture("android/assets/Screens/optionBETA.png"), 0, 0);
        backButton = new ImageActor(new Texture("android/assets/BTN/BACK.png"), 90, 100);
        backButton.addListener(new MoveToMainMenu());
        musikOn = new ImageActor(new Texture("android/assets/BTN/MUSIC.png"), 240, 450);
        musikOn.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                musikOn.setVisible(false);
                musikOff.setVisible(true);

            }
        });
        musikOff = new ImageActor(new Texture("android/assets/BTN/NoMUSIC.png"), 240, 450);
        musikOff.setVisible(false);
        musikOff.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                musikOff.setVisible(false);
                musikOn.setVisible(true);

            }
        });
        CatButton = new ImageActor(new Texture("android/assets/BTN/Cat.png"), 700, 175);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(musikOff);
        stage.addActor(backButton);
        stage.addActor(musikOn);
        stage.addActor(CatButton);

    }

    public void Music(InputEvent event, float x, float y) {
        PerfecatGame.getInstance().MoveToGame();
    }


    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    }

