package com.ads.perfecat.Screens;

import com.ads.perfecat.PerfecatGame;
import com.ads.perfecat.View.ImageActor;
import com.ads.perfecat.Screens.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by 05k1103 on 02.04.2016.
 */
/*public class UserInterface extends Stage {
    private ImageActor Korm;
    private ImageActor HochuIgrat;
    private ImageActor HochuSpat;
    private ImageActor Moloko;
    private ImageActor Myaso;
    private ImageActor Voda;
    private ImageActor wantToKorm;
    private ImageActor wantToMoloko;
    private ImageActor wantToIgrat;
    private ImageActor wantToMyaso;
    private ImageActor wantToSpat;
    private ImageActor wantToVoda;

    public UserInterface(ScreenViewport viewport, SpriteBatch batch) {
        super(viewport, batch);
        float cellCize = 160f;
        float tmpX = PerfecatGame.WORLD_WIDTH - cellCize;
        float tmpY = PerfecatGame.WORLD_HEIGHT / 5f;
        Korm = new ImageActor(new Texture("android/assets/Hochu/Korm.png"), 0, tmpY - cellCize/2);
        Korm.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                if (wantToKorm != null)
                    wantToKorm.remove();


        }});
        Moloko = new ImageActor(new Texture("android/assets/Hochu/Moloko.png"), 0, 2 * tmpY - cellCize/2);
        Moloko.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                if (wantToMoloko != null){
                    wantToMoloko.remove();}
            }
        });
        Myaso = new ImageActor(new Texture("android/assets/Hochu/Myaso.png"), 0, 3 * tmpY - cellCize/2);
        Myaso.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                if (wantToMyaso != null){
                    wantToMyaso.remove();}
            }
        });
        Voda = new ImageActor(new Texture("android/assets/Hochu/Voda.png"), 0, 4 * tmpY - cellCize/2);
        Voda.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {

                if (wantToVoda != null){
                    wantToVoda.remove();}
            }
        });
        HochuIgrat = new ImageActor(new Texture("android/assets/Hochu/HochuIgrat.png"), tmpX, 3 * tmpY - cellCize/2);
        HochuIgrat.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                if (wantToIgrat != null){
                    wantToIgrat.remove();}
            }
        });
        HochuSpat = new ImageActor(new Texture("android/assets/Hochu/HochuSpat.png"), tmpX, 4 * tmpY - cellCize/2);
        HochuSpat.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                if (wantToSpat != null){
                    wantToSpat.remove();}

            }
        });

        addActor(Korm);
        addActor(Moloko);
        addActor(Myaso);
        addActor(Voda);
        addActor(HochuIgrat);
        addActor(HochuSpat);
    }

    public void setWantToKorm(ImageActor wantToKorm) {
        this.wantToKorm = wantToKorm;
    }

    public void setWantToMoloko(ImageActor wantToMoloko) {
        this.wantToMoloko = wantToMoloko;
    }

    public void setWantToIgrat(ImageActor wantToIgrat) {
        this.wantToIgrat = wantToIgrat;
    }

    public void setWantToMyaso(ImageActor wantToMyaso) {
        this.wantToMyaso = wantToMyaso;
    }

    public void setWantToSpat(ImageActor wantToSpat) {
        this.wantToSpat = wantToSpat;
    }

    public void setwantToVoda(ImageActor wantToVoda) {
        this.wantToVoda = wantToVoda;
    }

}
*/
