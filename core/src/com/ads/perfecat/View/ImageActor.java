package com.ads.perfecat.View;

import com.ads.perfecat.PerfecatGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by PC on 05.03.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * PerfecatGame.getInstance().getPpuX(), y * PerfecatGame.getInstance().getPpuY());
        setSize(width * PerfecatGame.getInstance().getPpuX(), height * PerfecatGame.getInstance().getPpuY());
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public void draw(Batch batch, float parentAlpha){
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }

}
