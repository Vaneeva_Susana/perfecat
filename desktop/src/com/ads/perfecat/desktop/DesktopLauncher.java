package com.ads.perfecat.desktop;

import com.ads.perfecat.PerfecatGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ads.perfecat.perfecat;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name","EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "PERFECAT";
		config.width = 960;
		config.height = 540;
		new LwjglApplication(PerfecatGame.getInstance(), config);
	}
}
